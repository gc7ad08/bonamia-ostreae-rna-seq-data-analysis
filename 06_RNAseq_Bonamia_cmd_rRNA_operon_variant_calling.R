
library(RColorBrewer)

# load data

#tiff("all_runs_mpileup-g100000-d100000-A-Q0-aAD-aDP_call-m-ploidy1-g100000-A-FANAC.vcf_clean_for_R.tiff", units="in", width=30, height=6, res=1200)

dataFrame <- read.table("Contig_1_Bonamia_rRNA_operon.frequency.monomorphic.vcf.all.clean_for-missing_final2.txt", header=TRUE)

head(dataFrame)

# max value coverage to scale the graph

maxCov <- 400000

# plot the coverage on the positive strand

# 6 row for plot 1, 1 row for plot 2, 6 row for plot 3

layout(matrix(c(1,1,1,1,1,1,2,2,2,2,2,2,3,3,4,4,5,5,5,5,5,5,6,6,6,6,6,6), nrow = 28, ncol = 1, byrow = TRUE))

par(mar=c(.1, 5, .5, .1) + .5, oma=c(12,.1,3,1))

# plot 1 coverage RNAseq

#color from RColorBrewer
cols = brewer.pal(4, "Blues")
pal = colorRampPalette(cols)
dataFrame$order=findInterval(dataFrame$Couverture, sort(dataFrame$Couverture))

le <- length(dataFrame$Position)

#par(fig=c(0,1,0,0.2))
plot(dataFrame$Couverture,
     type='h',
     ylim=c(0,maxCov),
     xlim=c(0,le),
     col=pal(nrow(dataFrame))[dataFrame$order],
     ylab='Coverage',
     cex.lab = .7,
     xlab=NA,
     xaxt='n',
     main=NA,
     lwd=0.3,
     axes=F
     )

axis(2,
     at = seq(from = 0, to = maxCov, by = floor(maxCov/4)), cex.axis = .6
     )

# plot 2 SNP RNAseq

# calculate %
dataFrame$percRef <- (dataFrame$Reference/(dataFrame$Reference+dataFrame$Variant1+dataFrame$Variant2+dataFrame$Variant3))*100
dataFrame$percVar1 <- (dataFrame$Variant1/(dataFrame$Reference+dataFrame$Variant1+dataFrame$Variant2+dataFrame$Variant3))*100
dataFrame$percVar2 <- (dataFrame$Variant2/(dataFrame$Reference+dataFrame$Variant1+dataFrame$Variant2+dataFrame$Variant3))*100
dataFrame$percVar3 <- (dataFrame$Variant3/(dataFrame$Reference+dataFrame$Variant1+dataFrame$Variant2+dataFrame$Variant3))*100

# replace 0 by NA
dataFrame[dataFrame == 0] <- NA

head(dataFrame)

# plot 2 SNP RNAseq plot 1

plot(dataFrame$Position,dataFrame$percVar3,
     pch = 16,
     xaxt='n',
     col="green",
     axes=F,
     ylab='Percentage',
     cex=.5,
     ylim=c(0,100),
     )

# generate my y-axis
axis(2, line=0,
     at = seq(from = 0, to = 100, by = 20),
     labels = FALSE
     )

# to reverse % label
lablist<-as.vector(c(0,20,40,60,80,100))
text(seq(0, 100, by=20), labels = lablist, pos = 2, xpd = TRUE, offset=2.5, cex=0.6)


# plot 2 SNP RNAseq plot 2

par(new = T)
plot(dataFrame$Position,dataFrame$percVar2,
     pch = 16,
     col="red",
     axes=F,
     xlab="",
     ylab="",
     ylim=c(0,100),
     cex=.5
     )

# plot 2 SNP RNAseq plot 3

par(new = T)
plot(dataFrame$Position,dataFrame$percVar1,
     pch = 16,
     col="black",
     axes=F,
     xlab="",
     ylab="",
     ylim=c(0,100),
     cex=.5
     )

# plot 2 SNP RNAseq plot 4

par(new = T)
plot(dataFrame$Position,dataFrame$percRef,
     pch = 16,
     col="gray",
     axes=F,
     xlab="",
     ylab="",
     ylim=c(0,100),
     cex=.5
     )

# plot 3 annotation rRNA

dataFrame$V11 <- 125

plot(dataFrame$V11,
     type='h',
     ylim=c(0,125),
     col=NA,
     ylab=NA,
     xlab=NA,
     xaxt='n',
     main=NA,
     axes=F
     )

mtext("rRNA Operon", 2, las=1, adj = 1, outer=F, cex=.7)

rect(114,0,1921,125,col="coral1",border="black",lwd=.5)
rect(1922,0,2064,125,col="gold1",border="black",lwd=.5)
rect(2065,0,2205,125,col="darkolivegreen3",border="black",lwd=.5)
rect(2206,0,2368,125,col="goldenrod3",border="black",lwd=.5)
rect(2369,0,5480,125,col="deepskyblue2",border="black",lwd=.5)

# plot 4 annotation rRNA primer

plot(dataFrame$V11,
     type='h',
     ylim=c(0,125),
     col=NA,
     ylab=NA,
     xlab=NA,
     xaxt='n',
     main=NA,
     axes=F
     )

mtext("Primers", 2, las=1, adj = 1, outer=F, cex=.7)

rect(304,0,323,125,col="deeppink1",border="black",lwd=.5)
rect(5457,0,5476,125,col="deeppink1",border="black",lwd=.5)

# plot 5 coverage Nanopore

dataFrame2 <- read.table("PEPPER_MARGIN_DEEPVARIANT_FINAL_OUTPUT.g.vcf.clean.txt_final_for-R2.txt", header=TRUE)

head(dataFrame2)

#color from RColorBrewer
cols = brewer.pal(4, "Blues")
pal = colorRampPalette(cols)
dataFrame2$order=findInterval(dataFrame2$Couverture, sort(dataFrame2$Couverture))

# max value coverage to scale the graph

maxCov <- 1300

#coul <- brewer.pal(8, "Dark2") 

le <- length(dataFrame2$Position)

#par(fig=c(0,1,0.60,0.85), new=TRUE)
plot(dataFrame2$Couverture,
     type='h',
     ylim=c(0,maxCov),
     xlim=c(0,le),
     col=pal(nrow(dataFrame2))[dataFrame2$order],
     ylab='Coverage',
     cex.lab = .7,
     xlab=NA,
     xaxt='n',
     main=NA,
     lwd=0.3,
     axes=F
     )

axis(2,
     at = seq(from = 0, to = maxCov, by = floor(maxCov/4)), cex.axis = .6
     )

#plot 6 snp calling RNAseq

# calculate %
dataFrame2$percRef <- (dataFrame2$Reference/(dataFrame2$Reference+dataFrame2$Variant1))*100
dataFrame2$percVar1 <- (dataFrame2$Variant1/(dataFrame2$Reference+dataFrame2$Variant1))*100

# replace 0 by NA
dataFrame2[dataFrame2 == 0] <- NA

head(dataFrame2)

plot(dataFrame2$Position,dataFrame2$percVar1,
     pch = 16,
     xaxt='n',
     col="black",
     axes=F,
     ylab='Percentage',
     cex=.5,
     ylim=c(0,100),
     )

# generate my y-axis
axis(2, line=0,
     at = seq(from = 0, to = 100, by = 20),
     labels = FALSE
     )

# to reverse % label
lablist<-as.vector(c(0,20,40,60,80,100))
text(seq(0, 100, by=20), labels = lablist, pos = 2, xpd = TRUE, offset=2.5, cex=0.6)


# plot 2 SNP RNAseq plot 2

par(new = T)
plot(dataFrame2$Position,dataFrame2$percRef,
     pch = 16,
     col="gray",
     axes=F,
     xlab="",
     ylab="",
     ylim=c(0,100),
     cex=.5
     )

# generate my x-axis
l <- 5599
axis(1,
     at = seq(from = 0, to = l, by = floor(l/4)),
     labels = seq(from = 1,
                  to = 5599,
                  by = floor(l/4)
                  )
     )

# PDF output
dev.copy(pdf,"Contig_1_Bonamia_rRNA_operon.frequency.monomorphic.vcf.all.clean_for-missing_final2_test.pdf",
        width=15,
        height=10
        )

dev.off()