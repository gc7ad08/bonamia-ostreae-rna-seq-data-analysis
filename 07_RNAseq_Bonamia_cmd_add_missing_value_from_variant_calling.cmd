#!/usr/bin/env bash

# $1 Contig_1_Bonamia_rRNA_operon.frequency.monomorphic.vcf.all.clean_for-missing.txt

cp $1 test.txt

awk 'NR == 1 {print$0}' test.txt > 1.tmp

gsed -i '1d' test.txt

for ((i=1;i<=5599;i++))

do

	echo ""$i"_5599"

	position=`awk '$1 == '"$i"' {print$0}' test.txt`

	echo "$position"

	lineBefore=`awk 'NR == '"$i"'-1 {print$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10}' test.txt | gsed 's/\t/_/g'`

	echo "$lineBefore"

	if [[ -z "$position" ]]

	then

  		gsed -i ''$i'i'$i' '$lineBefore'' test.txt

  		gsed -i 's/_/\t/g' test.txt

  		gsed -i 's/ /\t/g' test.txt

  		gsed -i 's/\t\+/\t/g;s/^\t//' test.txt

	fi

done

cat 1.tmp test.txt > $1_final.txt

rm 1.tmp

rm test.txt 

