# Bonamia ostreae RNA-seq data analysis

Commands and functions to process the data and generate figures for the manuscript "De novo transcriptome assembly and analysis of the Flat Oyster pathogenic protozoa Bonamia ostreae" by Chevignon et al.

Author: Germain Chevignon germain.chevignon@ifremer.fr

Please note: the code can only be used to see how the data analyses were performed. It should not be treated as a ready-to-use software for transcriptome analysis.

The main reason for the code not being ready-to-use is that it was used in specific cluster environment.

Raw Illumina RNA sequencing reads, and raw Nanopore DNA sequencing reads are available at the Sequence Reads Archive (SRA) under accession PRJNA731671. The full-length B. ostreae rRNA operon is available under accession MZ305451. Transcriptome assembly presented here and accessory data associated with the assembly used for annotation and further analysis were uploaded to Figshare as file set (https://doi.org/10.6084/m9.figshare.16592480.v1 and https://doi.org/10.6084/m9.figshare.16598633.v1).
